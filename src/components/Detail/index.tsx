import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router';
import { Movie } from '../../utils/type';
import { getMovie } from '../../utils/API';
import { IMAGE_URL } from '../../utils/static';
import { format } from 'date-fns';
import { timeConvert } from '../../utils/helper';
import Loading from '../utils/loading';

const Detail = (): JSX.Element => {
  const history = useHistory();
  const [movie, setMovie] = useState<Movie | undefined>(undefined);
  const { id } = useParams();
  
  const fetchMovie = async () => {
    const movie = await getMovie(id);
    setMovie(movie);
  }
  
  useEffect(() => {
    fetchMovie();
  }, []);

  const formatDate = (d:string):string => {
    try{
      return format(new Date(release_date), "yyyy");
    }catch{
      return "";
    }
  }
  if (!movie) return <Loading />
  const { backdrop_path, original_title, overview,
    poster_path, release_date, vote_average, runtime } = movie;
  return (
    <>
      <div className="header detail" style={{
        backgroundImage: `url(${IMAGE_URL}${backdrop_path})`
      }}>
        <div className="col-11 mx-auto">
          <i className="fas fa-arrow-left fa-2x mt1" style={{ cursor: 'pointer' }} onClick={()=>{history.push("/")}}/>
        </div>
      </div>

      <div className="col-11 mx-auto mt1">
        <div className="flex mb4">
          <div className="detail-image">
            <img src={`${IMAGE_URL}${poster_path}`} />
          </div>
          <div className="ml2 movie-title">
            <h1 className="h1 bold">{original_title}</h1>
            <span className="mt1 info">{formatDate(release_date)} . {Math.ceil(vote_average * 10)}% User Score</span>
            <span className="info">{timeConvert(runtime)}</span>
          </div>
        </div>
        <hr />
        <h2 className="h2">Overview</h2>
        <div>{overview}</div>
      </div>
    </>
  );
}
export default Detail;
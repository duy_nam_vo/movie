import React, { useEffect, useState } from 'react';
import { getTrending, searchMovie } from '../../utils/API';
import { MediaSchema } from '../../utils/type';
import Header from './header';
import Tile from '../Tile';
import pluralize from 'pluralize';
import Search from './search';
import Loading from '../utils/loading';
const Home = (): JSX.Element => {
  const [mediaList, setMediaList] = useState<MediaSchema[]>([]);
  const [search, setSearch] = useState<string>("");
  const [searchType,setSearchType] = useState<"search"|"popular">("popular");
  const [isLoading,setIsLoading] = useState<boolean>(true);

  const fetchList = async () => {
    const response = await getTrending();
    setMediaList(response.results);
  }
  
  const fetchSearchMovie = async () => {
    const response = await searchMovie(search);
    setMediaList(response.results);
  }
  useEffect(() => {
    setIsLoading(true);
    if (search === "") fetchList();
    else fetchSearchMovie();
    setIsLoading(false);
  }, [search]);
  
  useEffect(()=>{
    if(search==="") setSearchType("popular");
    else setSearchType("search");
  },[mediaList]);

  const getHeaderTitle = () => searchType === "popular" ? "Popular Movies": `Found ${mediaList.length} ${pluralize('result',mediaList.length)}`;
  return (
    <>
      <Header />
      <Search setSearch={setSearch} search={search}/>
      {isLoading 
      ? 
        <Loading />
      : 
        <div className="col-11 mx-auto flex flex-column">
          <h2 className="h2 mt1">{getHeaderTitle()}</h2>
          <div className="mx-auto thumbnails">
            {mediaList.length > 0 &&
              mediaList.map(media => <Tile key={media.id} media={media} />)
            }
          </div>
        </div>
      }

    </>
  );
}

export default Home;
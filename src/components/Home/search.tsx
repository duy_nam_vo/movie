import React from 'react';

interface Props {
  search: string;
  setSearch(s: string): void;
}
const Search = ({ search, setSearch }: Props):JSX.Element => {
  return(<div className="col-6 mx-auto field">
    <p className="control has-icons-right">
      <input className="input is-rounded"
        type="text" placeholder="Search"
        value={search}
        onChange={(ev) => { setSearch(ev.target.value) }}
      />
      <span className="icon is-small is-right" style={{ color: '#01d277' }}>
        <i className="fas fa-search" />
      </span>
    </p>
  </div>);
}
export default Search;
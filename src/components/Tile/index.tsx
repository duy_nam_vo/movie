import React from 'react';
import { MediaSchema } from '../../utils/type';
import { format } from 'date-fns';
import Badge from '../utils/badge';
import { useHistory } from 'react-router';
import { IMAGE_URL } from '../../utils/static';
interface Props {
  media: MediaSchema
};

const Tile = ({ media }: Props) => {
  const history = useHistory();
  const getDateFormat = ():string => {
    try{
      return format(new Date(release_date), 'MMMM YYY');
    } catch{
      return "";
    }
  }
  const { id, vote_average, poster_path, title, release_date } = media;
  return (
    <div className="tile-image mr1 mb2 flex flex-column relative" onClick={() => { history.push(`/movie/${id}`) }}>
      <Badge value={Math.ceil(vote_average * 10)} />
      <img src={`${IMAGE_URL}/${poster_path}`} />
      <span className="subtitle mt1">{title}</span>
      <span className="subtitle-date">{getDateFormat()}</span>
    </div>
  );
}
export default Tile;
import React from 'react';
import clsx from 'clsx';
interface Props {
  value: number;
}

const Badge = ({ value }: Props): JSX.Element => {
  return (<div className={`${clsx(
    'badge', 
    value >= 80 && 'top',
    value < 80 && value > 50 && 'average',
    value <= 50 && 'bad',
  )}`}>
    {`${value}%`}
  </div>)
}
export default Badge;
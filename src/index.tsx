import React from "react";
import ReactDOM from "react-dom";
import './scss/app.scss';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Home from './components/Home';
import Detail from './components/Detail';

const App = (): JSX.Element => {
  return (
    <Router>
        <Switch>
          <Route path="/movie/:id">
            <Detail />
          </Route>
          {/* <Route path="/tv/:id">tv</Route> */}
          <Route path="/search">search</Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
    </Router>
  );
}

let element = document.getElementById("app");

ReactDOM.render(<App />, element);
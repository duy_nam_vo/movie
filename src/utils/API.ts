import { ApiResponse, Movie } from "./type";
const KEY = '6ed12e064b90ae1290fa326ce9e790ff';
const API_URL = 'https://api.themoviedb.org/3/';

const getTrending = async (): Promise<ApiResponse> => {
  const response = await fetch(`${API_URL}trending/movie/week?api_key=${KEY}`)
    .then(response => response.json());
  return response as ApiResponse;
}
const getMovie = async (id: number): Promise<Movie> => {
  const response = await fetch(`${API_URL}movie/${id}?api_key=${KEY}`)
    .then(response => response.json());
  return response as Movie;
}

const searchMovie = async(search:string) : Promise<ApiResponse> => {
  const response = await fetch(`${API_URL}search/movie?api_key=${KEY}&query=${encodeURI(search)}`)
  .then(response=>response.json());
  return response as ApiResponse;
}

export { getTrending, getMovie, searchMovie };
export interface MediaSchema {
  poster_path?:string;
  adult:boolean;
  overview:string;
  release_date:string;
  genre_ids:number[];
  id:number;
  original_title:string;
  original_language:string;
  title:string;
  backdrop_path?:string;
  popularity:number;
  vote_count:number;
  video:boolean;
  vote_average:number;
}

export interface ApiResponse {
  page:number;
  results: MediaSchema[];
  total_pages:number;
  total_results:number;
}

export interface Movie {
  adult:boolean;
  backdrop_path:string;
  belongs_to_collection:any;
  budget:number;
  genres:{id:number;name:string;}[];
  homepage?:string;
  id:number;
  imdb_id?:string;
  original_language:string;
  original_title:string;
  overview?:string;
  popularity:number;
  poster_path?:string;
  production_companies:{name:string;id:number;logo_path:string;origin_country:string;}[];
  production_countries:{iso_3166_1:string;name:string}[];
  release_date:string;
  revenue:number;
  runtime:number;
  spoken_language: {iso_639_1:string;name:string;}
  status:"Rumored"|"Planned"| "In Production" | "Post Production" 
  | "Released" | "Canceled";
  tagline:string;
  title:string;
  video:boolean;
  vote_average:number;
  vote_count:number;
}